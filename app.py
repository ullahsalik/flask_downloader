from flask import Flask, request, jsonify
import requests, uuid, os, time
from multiprocessing import Process, Manager

app = Flask(__name__)

process_list = []
process_dict = Manager().dict()


def file_download(url, file_name, id):
    start = time.process_time()
    response = requests.get(url, stream = True, allow_redirects=True)
    with open(file_name,"wb") as file:
        dl = 0
        for chunk in response.iter_content(chunk_size=1024):
            dl += len(chunk)
            process_dict['speed'] = (dl//(time.process_time() - start)/8)

            if chunk:
                file.write(chunk)

@app.route('/download', methods=['GET'])
def main():
    url = request.args.get('url')
    id = str(uuid.uuid4().hex[:5])
    try:
        filename = url.split('/')[-1]
        header = requests.head(url, allow_redirects=True).headers
    except Exception as e:
        return(jsonify({'error':'invalid url'}))
    process_dict[id]={
        'Total file size': int(header.get('content-length', None)),
        'File name': filename,
    }

    process = Process(name=str(id), target=file_download, args=(url, filename, id))
    process_list.append(process)
    process.daemon = True
    process.start()
    context = {
    'download_id' : id,
    }
    return(jsonify(context))

@app.route('/status', methods=['GET'])
def get_download_details():
    download_id = request.args.get('id')
    try:
        download_data = process_dict[download_id]
        d_file_size=os.path.getsize(download_data['File name'])
        r_file_size = download_data['Total file size'] - d_file_size
        eta = r_file_size/process_dict['speed']
        if r_file_size !=0:
            status = "Downloading"
        else:
            status = "Completed"
        download_data["Downloaded file size"] = "{}KB".format(round(((d_file_size)/1024),2))
        download_data["Remaining file size"] = "{}KB".format(round(((r_file_size)/1024),2))
        download_data["Status"] = status
        download_data["Estimated time"] = eta
        process_dict[download_id]=download_data
        return(process_dict[download_id])

    except Exception as e:
        return(jsonify({'error': 'Unable to find the download with particular id'}))

if __name__ == '__main__':
    app.run(debug=True)
